# -*- coding: utf-8 -*-
# This file is public domain according to its author, Brian Hsu

"""GitLab Snippet directive for reStructuredText."""

from nikola.utils import LOGGER
from nikola.plugin_categories import ShortcodePlugin


class Plugin(ShortcodePlugin):
    """Plugin for GitLab Snippet directive."""

    name = "snippet"

    def handler(self, namespace: str, project: str, snippet_id: str,
                gitlab_host='gitlab.com', **kw):
        """Create HTML for snippet"""
        embed_html = (f'<script src="https://{gitlab_host}/{namespace}/'
                     f'{project}/snippets/{snippet_id}.js"></script>')

        return embed_html, []
