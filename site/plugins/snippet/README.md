This plugins allows inclusion in pags and posts of GitLab Snippets

Using the power of ShortCodes, it's enough to include a call to the plugin which specifies gitlab namespace, project and snipped id:
```
:sc:`{{% snippets labs.kalfa.dev labs.kalfa.dev 2010992 %}}`
```
