# -*- coding: utf-8 -*-
# This file is public domain according to its author, Brian Hsu

"""Snippet directive for reStructuredText."""

from docutils.parsers.rst import Directive, directives
from docutils import nodes

from nikola.plugin_categories import RestExtension


class Plugin(RestExtension):
    """Plugin for snippet directive."""

    name = "rest_snippet"
    site = None

    def set_site(self, site):
        """Set Nikola site."""
        self.site = site
        directives.register_directive('snippet', GitLabSnippet)
        return super().set_site(site)


class GitLabSnippet(Directive):
    """Embed GitLab Snippet.

    Usage:

      .. snippet:: SNIPPET_ID NAMESPACE PROJECT

    or

      .. snippet:: SNIPPET_ID NAMESPACE PROJECT
         :gitlab_endpoint: GITLAB_HOST_ENDPOINT
    """
    required_arguments = 3
    optional_arguments = 1
    option_spec = {'gitlab_endpoint': directives.unchanged}
    final_argument_whitespace = True
    has_content = False

    def run(self):
        """Run the snippet directive."""
        namespace = self.arguments[0].strip()
        project = self.arguments[1].strip()
        snippet_id = self.arguments[2].strip()
        gitlab_endpoint = self.options.get('gitlab_endpoint', 'gitlab.com')

        embed_html = (f'<script src="https://{gitlab_endpoint}/{namespace}/'
                      f'{project}/snippets/{snippet_id}.js"></script>')

        return [nodes.raw('', embed_html, format='html')]
