.. title: Let's celebrate a first post (also my first Nikola plugin)
.. slug: nikola
.. date: 2020-09-02 13:20:07 UTC+01:00
.. tags: 
.. category:
.. status: 
.. link: 
.. description: how I am using Nikola and GitLab CI
.. type: text

Intro
-----
Few weeks ago I decided to start a new blog.

I use python every day at work, full time. I have been doing it for years. When I decided to start a new blog about what I am doing in the technological side of my life, it came naturally to decide to use tools in the python ecosystem.

I needed something that is plain text oriented, does not require a large system to run and I can store easily, possibly as code.

Nikola is the first tool which came to my mind. Hugo is written in Go, and for how much I consider Go a cool language, I am not writing anything in Go lately (with the exception of few terraform experiments I will keep for another post).

`Nikola <https://getnikola.com/>`__ is a really cool tool, simple and extensible, which allows publishing blog and static pages in a very straightforward way.

I am not a web developer, I mostly deal with network programming, althought in the last years I have been working on modelling in python the world of `Analog IP Design <https://www.agileanalog.com/>`__, and I have tried to stay fairly away from the web development world during my entire career.

Using a platform like Nikola allows me to have direct control of what I am doing, without having to care about the publishing details, being it CSS, JavaScript or simply HTML templating.

Using it is quite easy, the most trouble I had has been creating a pages-only site, i.e. no blog posts, for my `code page <https://labs.kalfa.dev>`__ (which is still under construction).

To make it more interesing I decided to play with GitLab CI and GitLab Pages, so that GitLab publishes the statically generated site for me.

Building the blog
-----------------
GitLab CI is pretty straightfoward in its basic architecture. You have jobs, jobs have artefacts, the pipeline publishes artefacts according to what kind of job it is.

There is nothing else to do than adding a job to build the site and tell the pipeline where are th build pages.

The only magic is done by the special job name **pages**, which publishes its artifacts to *gitlab.io*, which can be mapped to any domain in GitLab Pages settings, using Let's Encrypt for HTTPS.

:sc:`{{% listing nikola_gtlab_ci.yaml yaml linenumbers=True %}}`

Where the gitlab deploy task is defined in the Nikola conffile as

.. snippet:: labs.kalfa.dev labs.kalfa.dev 2010992 

New Plugin and RsT directive
----------------------------

By using GitLab for my code, I also use some of the services around it as well, including snippets.

Well, Nikola does not seem to have anything which can interface with GitLab and show snippets, but it has one for GitHub Gist.

So, I took inspiration from the ShortCode and Directive for Gist, and added a basic implementation for its GitLab equivalent. In fact, the snippet above is using the new directive.

I will soon start a merge request to Nikola plugins. Meanwhile `the code <https://gitlab.com/labs.kalfa.dev/blogs.kalfa.dev/-/tree/master/site/plugins>`__ is in GitLab
